﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    private float timeCounter;
    private float timeToShoot;

    private float timeShooting;
    private float speedx;

    private bool isShooting;

    private ScoreManager sm;

    public int puntuacion = 1000;

    private bool isStage1 = true;

    private bool isStage2 = false;

    private int health = 100;

    [SerializeField] GameObject bullet;

    //private int bulletAngle = 180;
    private float bulletAngle = Mathf.PingPong(Time.time, 180) - 90;

    private void Awake() {
     
        sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();

        Inicitialization();
    }

    protected virtual void Inicitialization(){
        timeCounter = 0.0f;
        timeToShoot = 1.0f;
        timeShooting = 3.0f;
        speedx = 3.0f;
        isShooting = false;
    }

    protected virtual void BossBehaviourStage1(){

       timeCounter += Time.deltaTime;

       if(timeCounter>timeToShoot){
               
         Instantiate(bullet,this.transform.position,Quaternion.Euler(0,0, bulletAngle + Time.time*4),null);
          
            if(timeCounter>(timeToShoot+timeShooting)){
                timeCounter = 0.0f;
                isShooting = false;
            }
        }
        

    }

    protected virtual void BossBehaviourStage2(){
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if ((transform.position.x > 7) && isStage1 ){
            transform.Translate(-speedx*Time.deltaTime,0,0);
        }
        
        if (transform.position.x < 7){
        BossBehaviourStage1();
        }

    }




  
   
}
