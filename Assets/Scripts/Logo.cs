﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Logo : MonoBehaviour
{
    private Image image;
    private float alpha = 0f;
    private float timeCounter= 0f;
    bool isFadeIn = true;

    void Start()
    {
          image = GetComponent<Image>();
    }
    

    void Update ()
    {
        
        if (isFadeIn){
            alpha += Time.deltaTime/2;
            image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
           
            if (alpha >= 1f){
            isFadeIn = false;
        }
        }
      

        if (!isFadeIn){ 
            timeCounter += Time.deltaTime;
            if (timeCounter >1f){
            alpha -= Time.deltaTime/2;
            image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
            }
        }
        if ((!isFadeIn && alpha <= 0) || Input.anyKey){
            SceneManager.LoadScene("MainMenu");
        }

    }
     
       
    
     

    
   

}
