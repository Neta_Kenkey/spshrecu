﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorcidWeapon : Weapon
{

    public GameObject torcidBullet;
    public float cadencia;

    public AudioSource audioSource;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate (torcidBullet, this.transform.position, Quaternion.identity, null);
        audioSource.Play();
    }
}
