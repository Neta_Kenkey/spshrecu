﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    public AudioSource resume;
    public AudioSource leave;


    private bool isPaused = false;
    [SerializeField] GameObject canvas;


    public void Continue(){
        leave.Play();
        canvas.SetActive(false);
        Time.timeScale = 1.0f;
        isPaused = false;
    }

    public void Quit(){
        leave.Play();
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("MainMenu");
    }


    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if (!isPaused && Input.GetKeyDown(KeyCode.Escape))
        {
            ActivatePause();
            resume.Play();
        }
        else if(isPaused && Input.GetKeyDown(KeyCode.Escape)){
            leave.Play();
            Continue();
        }
    }

    void ActivatePause(){
        isPaused = true;

        canvas.SetActive(true);

        Time.timeScale = 0;
    }
}
