﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallMeteor : Meteor
{
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish")
        {
            Destroy(this.gameObject);
        }
    }
}
