﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GOManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI score;
	[SerializeField] TextMeshProUGUI record;

    int m_score1;
    int recor;

    void Start()
    {
        SetText();
        record.text = PlayerPrefs.GetInt("Record", 0).ToString();
    }

    void SetText()
    {
        m_score1 = PlayerPrefs.GetInt("Score1", 0);
		ScoreText(m_score1);
        RecordText(recor);
    }

    public void ScoreText(int value){
        score.text = m_score1.ToString();
    }

    public void RecordText(int value)
    {
        score.text = m_score1.ToString();
        if (m_score1 > PlayerPrefs.GetInt("Record", 0))
        {
            PlayerPrefs.SetInt("Record", m_score1);
        }
    }

    public void Reset()
    {
        PlayerPrefs.DeleteAll();
        record.text = "000000";
    }

    public void PulsaPlayAgain(){
        SceneManager.LoadScene("Game");
    }

    public void PulsaExit(){
        SceneManager.LoadScene("MainMenu");
    }
}