﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PUManager : MonoBehaviour
{
    public Sprite Botiquín;

    public float HealsTimer;

    private float currentTime = 0;

    void Awake()
    {
        StartCoroutine(Heals());
    }
    public void OnTriggerEnter2D (Collider2D other) {
                if (other.tag == "Player") {
                    GameObject WinLifes = GameObject.Find("Ship");
                    PlayerBehaviour playerBehaviour = WinLifes.GetComponent<PlayerBehaviour>();
                    playerBehaviour.lives++;
                    Destroy(gameObject);
                }
            }

    IEnumerator Heals()
    {
        while (true)
        {
            Instantiate(Botiquín, new Vector3(10, Random.Range(-5, 5)), Quaternion.identity, this.transform);
            yield return new WaitForSeconds(HealsTimer);
        }
        
    }

}
